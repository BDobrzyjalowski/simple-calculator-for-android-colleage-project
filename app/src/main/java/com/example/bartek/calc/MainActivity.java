package com.example.bartek.calc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    TextView textB;
    String number_one = "";
    String number_two = "";
    Boolean dot=false;
    Boolean plus_minus_button = false;
    double one = 0;
    double two = 0;
    Boolean first_number = true;
    int number_of_operation = 0;

    public void PlusOrMinus()
    {
        if (plus_minus_button == true)
        {
            double tmp;


            tmp = Double.parseDouble(textB.getText().toString());
            tmp = tmp * (-1);
            textB.setText(Double.toString(tmp));
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textB = (TextView)findViewById(R.id.TextView01);
    }


    public void button1_Click(View view)
    {
        if (first_number == true)
        {
            number_one += "1";
            System.out.print("LISTUJE W CHUJ" + number_one);
            textB.setText(number_one);
        }
        else
        {
            number_two += "1";
            textB.setText(number_two);
        }
        PlusOrMinus();
    }

    public void button2_Click(View view)
    {
        if (first_number == true)
        {
            number_one += "2";
            textB.setText(number_one);
        }
        else
        {
            number_two += "2";
            textB.setText(number_two);
        }
        PlusOrMinus();
    }

    public void button3_Click(View view)
    {
        if (first_number == true)
        {
            number_one += "3";
            textB.setText(number_one);
        }
        else
        {
            number_two += "3";
            textB.setText(number_two);
        }
        PlusOrMinus();
    }

    public void button4_Click(View view)
    {
        if (first_number == true)
        {
            number_one += "4";
            textB.setText(number_one);
        }
        else
        {
            number_two += "4";
            textB.setText(number_two);
        }
        PlusOrMinus();
    }

    public void button5_Click(View view)
    {
        if (first_number == true)
        {
            number_one += "5";
            textB.setText(number_one);
        }
        else
        {
            number_two += "5";
            textB.setText(number_two);
        }
        PlusOrMinus();
    }

    public void button6_Click(View view)
    {
        if (first_number == true)
        {
            number_one += "6";
            textB.setText(number_one);
        }
        else
        {
            number_two += "6";
            textB.setText(number_two);
        }
        PlusOrMinus();
    }

    public void button7_Click(View view)
    {
        if (first_number == true)
        {
            number_one += "7";
            textB.setText(number_one);
        }
        else
        {
            number_two += "7";
            textB.setText(number_two);
        }
        PlusOrMinus();
    }

    public void button8_Click(View view)
    {
        if (first_number == true)
        {
            number_one += "8";
            textB.setText(number_one);
        }
        else
        {
            number_two += "8";
            textB.setText(number_two);
        }
        PlusOrMinus();
    }

    public void button9_Click(View view)
    {
        if (first_number == true)
        {
            number_one += "9";
            textB.setText(number_one);
        }
        else
        {
            number_two += "9";
            textB.setText(number_two);
        }
        PlusOrMinus();
    }

    public void button0_Click(View view)
    {
        if (first_number == true)
        {
            number_one += "0";
            textB.setText(number_one);
        }
        else
        {
            number_two += "0";
            textB.setText(number_two);
        }
        PlusOrMinus();
    }

    public void buttonDot_Click(View view)
    {
        if(dot==false)
        {
            if (first_number == true)
            {
                number_one += ".";
                textB.setText(number_one);
                dot=true;
            }
            else
            {
                dot=true;
                number_two += ".";
                textB.setText(number_two);
            }
            PlusOrMinus();
        }

    }

    public void buttonClear_Click(View view)
    {
        dot=false;
        number_one = "";
        number_two = "";
        textB.setText("");
        one = 0;
        two = 0;
        first_number = true;
        plus_minus_button = false;
    }

    public void buttonPlusMinus_Click(View view)
    {
        if (plus_minus_button == false)
        {
            plus_minus_button = true;
            //String x = textB.getText()

        }
        else
        {
            plus_minus_button = false;

        }
        double tmp = Double.parseDouble(textB.getText().toString());
        tmp=tmp*(-1);
        textB.setText(Double.toString(tmp));
    }

    public void buttonPlus_Click(View view)
    {
        if (number_two.equals("") == false)
        {
            double result = 0;
            two = Double.parseDouble(number_two);
            if (plus_minus_button == true)
            {
                two = two * (-1);
            }
            switch (number_of_operation)
            {
                case 1:
                    result = one + two;
                    break;
                case 2:
                    result = one - two;
                    break;
                case 3:
                    result = one * two;
                    break;
                case 4:
                    if (two != 0)
                    {
                        result = one / two;
                    }
                    break;
                case 5:
                    result = Math.pow(one, two);
                    break;
                default:
                    break;
            }
            textB.setText(Double.toString(result));
            one = result;
            number_of_operation = 1;
            first_number = false;
            number_two = "";
        }
        else
        {
            number_of_operation = 1;
            first_number = false;
            one = Double.parseDouble(number_one);
            if (plus_minus_button == true)
            {
                one = one * (-1);
            }
        }
        dot=false;
        plus_minus_button = false;
    }

    public void buttonMinus_Click(View view)
    {
        if (number_two.equals("") == false)
        {
            double result = 0;
            two = Double.parseDouble(number_two);
            if (plus_minus_button == true)
            {
                two = two * (-1);
            }
            switch (number_of_operation)
            {
                case 1:
                    result = one + two;
                    break;
                case 2:
                    result = one - two;
                    break;
                case 3:
                    result = one * two;
                    break;
                case 4:
                    if (two != 0)
                    {
                        result = one / two;
                    }
                    break;
                case 5:
                    result = Math.pow(one, two);
                    break;
                default:
                    break;
            }
            textB.setText(Double.toString(result));
            one = result;
            number_of_operation = 2;
            first_number = false;
            number_two = "";
        }
        else
        {
            number_of_operation = 2;
            first_number = false;
            one = Double.parseDouble(number_one);
            if (plus_minus_button == true)
            {
                one = one * (-1);
            }
        }
        dot=false;
        plus_minus_button = false;
    }

    public void buttonMultiply_Click(View view)
    {
        if (number_two.equals("") == false)
        {
            double result = 0;
            two = Double.parseDouble(number_two);
            if (plus_minus_button == true)
            {
                two = two * (-1);
            }
            switch (number_of_operation)
            {
                case 1:
                    result = one + two;
                    break;
                case 2:
                    result = one - two;
                    break;
                case 3:
                    result = one * two;
                    break;
                case 4:
                    if (two != 0)
                    {
                        result = one / two;
                    }
                    break;
                case 5:
                    result = Math.pow(one, two);
                    break;
                default:
                    break;
            }
            textB.setText(Double.toString(result));
            one = result;
            number_of_operation = 3;
            first_number = false;
            number_two = "";
        }
        else
        {
            number_of_operation = 3;
            first_number = false;
            one = Double.parseDouble(number_one);
            if (plus_minus_button == true)
            {
                one = one * (-1);
            }
        }
        dot=false;
        plus_minus_button = false;
    }

    public void buttonDivide_Click(View view)
    {
        if (number_two.equals("") == false)
        {
            double result = 0;
            two = Double.parseDouble(number_two);
            if (plus_minus_button == true)
            {
                two = two * (-1);
            }
            switch (number_of_operation)
            {
                case 1:
                    result = one + two;
                    break;
                case 2:
                    result = one - two;
                    break;
                case 3:
                    result = one * two;
                    break;
                case 4:
                    if (two != 0)
                    {
                        result = one / two;
                    }
                    break;
                case 5:
                    result = Math.pow(one, two);
                    break;
                default:
                    break;
            }
            textB.setText(Double.toString(result));
            one = result;
            number_of_operation = 4;
            first_number = false;
            number_two = "";
        }
        else
        {
            number_of_operation = 4;
            first_number = false;
            one = Double.parseDouble(number_one);
            if (plus_minus_button == true)
            {
                one = one * (-1);
            }
        }
        dot=false;
        plus_minus_button = false;
    }

    public void buttonResult_Click(View view)
    {
        double result = 0;

        if (plus_minus_button == true)
        {
            two = two * (-1);
        }
        if(number_two.equals("") == false) {
            two = Double.parseDouble(number_two);
            switch (number_of_operation) {
                case 1:
                    result = one + two;
                    break;
                case 2:
                    result = one - two;
                    break;
                case 3:
                    result = one * two;
                    break;
                case 4:
                    if (two != 0) {
                        result = one / two;
                    }
                    break;
                case 5:
                    result = Math.pow(one, two);
                    break;
                default:
                    break;
            }
            textB.setText(Double.toString(result));
            one = result;
            number_one = Double.toString(one);
            first_number = false;
            number_two = "";

            plus_minus_button = false;
        }
        dot=false;

    }

    public void buttonSqrt_Click(View view)
    {

        if (plus_minus_button == false)
        {
            double result = 0;
            one = Double.parseDouble(number_one);
            result = Math.sqrt(one);
            textB.setText(Double.toString(result));
            one = result;
            first_number = false;
            number_one = Double.toString(one);
            number_two = "";
            plus_minus_button = false;
            dot=false;
        }


    }

    public void buttonPower_Click(View view)
    {
        if (number_two.equals("") == false)
        {
            double result = 0;
            two = Double.parseDouble(number_two);
            if (plus_minus_button == true)
            {
                two = two * (-1);
            }
            switch (number_of_operation)
            {
                case 1:
                    result = one + two;
                    break;
                case 2:
                    result = one - two;
                    break;
                case 3:
                    result = one * two;
                    break;
                case 4:
                    if (two != 0)
                    {
                        result = one / two;
                    }
                    break;
                case 5:
                    result = Math.pow(one, two);
                    break;


                default:
                    break;
            }
            textB.setText(Double.toString(result));
            one = result;
            number_of_operation = 5;
            first_number = false;
            number_two = "";
        }
        else
        {
            number_of_operation = 5;
            first_number = false;
            one = Double.parseDouble(number_one);
            if (plus_minus_button == true)
            {
                one = one * (-1);
            }
        }
        dot=false;
        plus_minus_button = false;
    }
}
